django==3.1.7
psycopg2-binary==2.8.6
gunicorn==20.0.4
boto3==1.17.23
django-storages==1.11.1