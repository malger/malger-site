from django.test import SimpleTestCase
from django.urls import reverse


class AboutPageTest(SimpleTestCase):
    """
    Test the home/about page view
    """

    def test_about_page_status_code(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_about_page_url_name(self):
        response = self.client.get(reverse('about'))
        self.assertEqual(response.status_code, 200)

    def test_about_page_returns_correct_html(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'about.html')
