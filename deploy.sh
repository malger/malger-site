#!/bin/sh

ssh -o StrictHostKeyChecking=no ubuntu@$EC2_PUBLIC_IP_ADDRESS << 'ENDSSH'
  cd /home/ubuntu/mysite
  export $(cat .env | xargs)
  docker login $CI_REGISTRY -u $CI_REGISTRY_USER -p $CI_JOB_TOKEN
  docker pull $IMAGE:web
  docker pull $IMAGE:nginx
  docker-compose up -d
ENDSSH